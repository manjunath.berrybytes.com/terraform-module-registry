**Publish a Terraform module by using CI/CD inside the Gitlab Infra Registry**

- This repository is used for storing the Terraform module inside the Infrastructure registry of Gitlab.

- We are uploading the module inside gitlab using gitlab Ci/Cd.

- To work with Terraform modules in GitLab CI/CD, you can use **CI_JOB_TOKEN** in place of the personal access token in your commands.

- CI_JOB_TOKEN : it is a predefined variable, we do not need to provide any value for it.

**What does this module do?**
 - This module simply generates a local file which has been used here as an example.
 - The module consists of the following three files:
        
        main.tf

        outputs.tf
        
        variables.tf

 - This module is uploaded inside the Gitlab infrastructure registry.

**How does the ci/cd works**
 - We have a single phase: **upload** in the yml file.
 
 - This stage in turn is calling an internal file **Terraform-Module-Upload.gitlab-ci.yml**.
 
 - This file carries the command for packaging the module and then uplaoding it to git.
 
 - All the predefined variables are being used in this file.

 **NOTE:** To confirm whether your module is uplaoded or not just see the following message in the stage output.
     
     {
       "message":"201 Created"
     }

Also, goto Packages and registries > Infrastructure Registry > check if your module is displaying here.